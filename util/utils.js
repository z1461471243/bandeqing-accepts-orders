import qs from 'qs';
export default {
    /*防抖*/
    debounce(func, delay = 1000, immediate = false) {
        //闭包
        let timer = null
        //不能用箭头函数
        return function () {
            if (timer) {
                clearTimeout(timer)
            }
            if (immediate && !timer) {
                func.apply(this, arguments)
            }
            timer = setTimeout(() => {
                func.apply(this, arguments)
            }, delay)
        }
    },
    /*节流*/
    throttle(fn, delay = 1000) {
        //距离上一次的执行时间
        let lastTime = 0
        return function () {
            let _this = this
            let _arguments = arguments
            let now = new Date().getTime()
            //如果距离上一次执行超过了delay才能再次执行
            if (now - lastTime > delay) {
                fn.apply(_this, _arguments)
                lastTime = now
            }
        }
    },
    /*跳转*/
    goPages(url, auth) {
        // 判断是否需要登录
        if (auth) {
            uni.navigateTo({
                url
            })
        } else {
            uni.navigateTo({
                url,
                success(res) {
                    console.log("页面跳转", res)
                }
            })
        } 
    }, 
	urlToObj(url){
		return qs.parse(url);
	},
	objToUrl(obj){
		return qs.stringify(obj);
	} 
}