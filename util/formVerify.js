var setPasswordFrom = {
	oldPwd: {
		required: {
			message: "请输入当前密码",
		},
	},
	newPwd:{
		required: {
			message: "请输入新密码",
		},
	},
	newPwdConfirm:{
		required: {
			message: "请重复输入新密码",
		},
	},
	"name": {
		required: {
			message: "请输入姓名！",
		},
	},
	"phone": {
		required: {
			message: "请输入联系方式！",
		},
		min: {
			min: 11,
			message: "请输入不少于11位的联系号码",
		},
		format: {
			regex: /^[1][3,4,5,7,8][0-9]{9}$/,
			message: "请输入正确格式的联系方式"
		},
	},
	"address": {
		required: {
			message: "请输入详细地址",
		},
	},
}
module.exports = {
	setPasswordFrom
}