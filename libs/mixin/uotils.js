import dayjs from 'dayjs';

export default {
	data() {
		return {};
	},
	methods: {
		setKeyInValue(key) {
			var that = this;
			var userInfo = this.$data.userInfo; 
			/* 字典对应 */
			let sum = {
				workerSex: 'worker_sex', //性别
				workerLevel: 'worker_level', //等级
				workerStatus: 'worker_status', //账号状态
				workerType: 'worker_type', //类型
				workerNation: 'worker_nation', //民族
				workerSkills: 'worker_skills', // 技能
				workerTools: 'worker_tools', //工具
				workerCity: "worker_city", //城市
			}; 
			if (sum[key]) {
				if (userInfo[key] === "") {
					return "暂无";
				}
				let list = this.$store.state[sum[key]];
				if (list == undefined) {
					return userInfo[key];
				}
				for (let i in list) {
					if (list[i].value == userInfo[key]) {
						return list[i].label;
					}
				}
			} else {
				return userInfo[key]
			}
		},
		openPOP(key) {
			this.$data[key] = true;
		},
		setImagesUrl(url) {
			const baseUrl = 'https://bdq-cloud.oss-cn-shenzhen.aliyuncs.com/';
			if (url) {
				return baseUrl + url
			} else {
				return "../../../static/default_avatar.png";
			}
		},
		/* 时间格式化 */
		setTime(time) {
			if (time) {
				return dayjs(time).format('YYYY-MM-DD hh:mm:ss');
			}
			return '暂无时间';
		},
		/*防抖*/
		debounce(func, delay = 1000, immediate = false) {
			//闭包
			let timer = null
			//不能用箭头函数
			return function() {
				if (timer) {
					clearTimeout(timer)
				}
				if (immediate && !timer) {
					func.apply(this, arguments)
				}
				timer = setTimeout(() => {
					func.apply(this, arguments)
				}, delay)
			}
		},
		/*节流*/
		throttle(fn, delay = 1000) {
			//距离上一次的执行时间
			let lastTime = 0
			return function() {
				let _this = this
				let _arguments = arguments
				let now = new Date().getTime()
				//如果距离上一次执行超过了delay才能再次执行
				if (now - lastTime > delay) {
					fn.apply(_this, _arguments)
					lastTime = now
				}
			}
		},
		/*跳转*/
		goPages(url, auth = false) {
			if (!url) {
				return;
			}
			// 判断是否需要登录 
			uni.navigateTo({
				url,
				success(res) {

				}
			})

		}
	}
}