export default {
	// 下拉刷新
	refresherrefresh(type) {
		let that = this;
		let keyName = "refresherTriggered" + type;
		let _keyName = "_refresherTriggered" + type; 
		if (that[_keyName]) {
			return;
		}
		that[_keyName] = true;
		//界面下拉触发，triggered可能不是true，要设为true
		if (!that[keyName]) {
			that[keyName] = true;
		}
		that.loadStoreData(type);
	},
	refresherrestore(type) {
		console.log('自定义下拉刷新被复位');
		this.refresherabort(type);
	},
	refresherabort(type) {
		let that = this;
		that['refresherTriggered' + type] = false;
		that['_refresherTriggered' + type] = false;
	},
	/* 下拉刷新 */
	loadStoreData(type) {
		let that = this;
		let num = ['waitOrderPostData', 'proceedOrderPostData', 'successOrderPostData'];
		let listNum = ['waitOrderList', 'proceedOrder', 'successOrder'];
		this.$data[num[type]].page = 1;
		that.getData(this.$data[num[type]]);
		setTimeout(() => {
			let keyName = 'refresherTriggered' + type
			let _keyName = '_refresherTriggered' + type
			that[keyName] = false; //触发onRestore，并关闭刷新图标
			that[_keyName] = false;
		}, 1000);
	},
	/* 上拉刷新 */
	getScrollList(type) {
		var that = this;
		let num = ['waitOrderPostData', 'proceedOrderPostData', 'successOrderPostData'];
		let listNum = ['waitOrderList', 'proceedOrder', 'successOrder'];
		this.$data[num[type]].page += 1;
		that.getData(this.$data[num[type]]);
	},
}