/*
 * 文件名：vue混入 公共封装文件
 * 日期：20230113
 * 作者：张
 */ 
export default {
	onShow() {
		 
	},
	methods: {
		/* 拨打电话 */
		phoneCall(phone){
			uni.makePhoneCall({
				phoneNumber: phone //仅为示例
			});
		},
		 
		goBack() {
			uni.navigateBack();
		},
		/* 返回 首页 */
		goHome() {
			uni.reLaunch({
				url: "/pages/index/index",
			})
		},
		// 前往指定页面
		goPages(path) {
			if (path == "") {
				uni.showToast({
					title: "当前功能尚未开放，敬请期待。",
					icon: "none",
				})
				return;
			}
			if (path) {
				uni.navigateTo({
					url: path
				})
			}
		}
	},
	/* 获取一个随机数  n 最小值 m最大值 */
	getRandom() {
		return Math.floor(Math.random() * (m - n + 1)) + n;
	}
};