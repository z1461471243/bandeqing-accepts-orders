import http from '../request.js';
export default {
	/* 提交注册信息 */
	registerSubmit(postData) {
		return http.post("/worker/register", postData);
	},
	/* 上传图片信息 */
	attachmentUpload(postData) {
		return http.post("/attachment/upload", postData);
	},
	login(postData) {
		return http.post('/worker/login', postData);
	}
}