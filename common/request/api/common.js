import http from '../request.js';
import store from '../../../store/index.js';
export default {
	setStoeData(postData) {
		store.commit("$tStore", {
			name: postData.key,
			value: postData.data,
		});
		uni.setStorage({
			key: postData.key,
			data: postData.data
		})
	},
	/* 上传设备信息 */
	uploadEquipment(postData) {
		console.log(postData);
		return http.post("/worker/upload/equipment", postData)
	},
	/* 数据枚举接口 */
	dataEnum(postData) {
		var that = this;
		return new Promise((resolve, reject) => {
			/* 判断是否有缓存 */
			http.post("/config/dict", postData).then(res => {
				resolve(res);
				if (res.errorCode == 200) {
					that.setStoeData({
						key: postData.key,
						data: res.data
					})
				}
			}).catch((err) => {
				reject(err)
				console.log("请求失败", err)
			})

		}).catch(err => {
			reject(err);
		})
	},
	/*获取小哥类型枚举*/
	getWorkerType() {
		return this.dataEnum({
			key: 'worker_type'
		});
	},
	/*获取小哥状态类型枚举*/
	getWorkerStatus() {
		return this.dataEnum({
			key: 'worker_status'
		});
	},
	/* 获取小哥等级类型枚举 */
	getWorkerLevel() {
		return this.dataEnum({
			key: 'worker_level'
		});
	},
	/*获取小哥性别类型枚举*/
	getWorkerSex() {
		return this.dataEnum({
			key: 'worker_sex'
		});
	},
	/* 钱包操作类型 */
	getConfigWWRType() {
		return this.dataEnum({
			key: 'wwr_type'
		});
	},
	/*获取民族枚举*/
	getWorkerNation() {
		return this.dataEnum({
			key: 'worker_nation'
		});
	},
	/*获取店铺列表*/
	getStoreType() {
		return this.dataEnum({
			key: 'store_type'
		});
	},
	getConfigCitySelect(postData) {
		var that = this;
		return new Promise((resolve, reject) => {
			http.post("/config/citySelect", postData).then(res => {
				resolve(res);
				if (res.errorCode == 200) {
					that.setStoeData({
						key: 'worker_city',
						data: res.data
					})
				}
			}).catch(err => {
				reject(err);
			})
		})
	},
	/*获取城市*/
	getConfigCity(postData) {
		var that = this;
		return new Promise((resolve, reject) => {
			http.post("/config/city", postData).then(res => {
				resolve(res);
				if (res.errorCode == 200) {
					that.setStoeData({
						key: 'worker_city',
						data: res.data
					})
				}
			}).catch(err => {
				reject(err);
			})
		})
	},

}