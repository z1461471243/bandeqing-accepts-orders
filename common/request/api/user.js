import http from '../request.js';
export default {
	currnet(postData) {
		return new Promise((resolve, reject) => {
			http.post("/worker/currnet", postData).then(res => { 
				if (res.errorCode == 200) {
					uni.setStorage({
						key: "currnetInfo",
						data: res.data
					})
				}
				resolve(res);
			}).catch(err => {
				reject(err);
			})
		})
	},
	/* 我的工作日程 */
	getWorkTimeList(postData){
		return http.post("/workTime/list", postData) 
	},
	/* 开关日程 */
	setWorkTime(postData){
		return http.post("/workTime/modify", postData)
	},
	/* 钱包余额 */
	getWorkerWallet(postData){
		return http.post("/worker/wallet", postData) 
	},
	/* 钱包流水 */
	getWorkerWalletList(postData){
		return http.post("/worker/wallet/list", postData) 
	},
	/* 修改密码 */
	setWorkerPassword(postData){
		return http.post("/worker/modifyPwd", postData) 
	},
	
}