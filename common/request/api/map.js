/*地图搜索*/
export default {


    mapSearch(postData) {
        return new Promise((resolve, reject) => {
            uni.request({
                data: {
                    key: 'dc9b77f58c4cb50f57587792ca89cbeb',
                    keywords: postData.value
                },
                url: "https://restapi.amap.com/v3/place/text",
                success(res) {
                    console.log('地图搜索', res);
                    if (res.data.status == '1') {
                        resolve(res.data.pois);
                    } else {
                        reject({
                            code: 500, msg: "搜索失败,没有找到对应地址"
                        })
                    }
                }, fail(res) {
                    reject({
                        code: 500, msg: "请求错误，请重试。"
                    })
                }
            })
        })

    }


}