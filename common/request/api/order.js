import http from '../request.js';
export default {
	 /* 订单详情*/
	 dispatchDetail(postData){
		 return http.post("/dispatch/detail",postData)
	 },
	 /* 接单处理 */
	 dispatchHandle(postData){
		 return http.post("/dispatch/handle",postData)
	 },	 
}