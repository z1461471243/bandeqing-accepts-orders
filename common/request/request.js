import Request from '@/tuniao-ui//libs/luch-request'
import config from '../config.js'
const http = new Request({ 
	baseURL: config.baseURL,
	header: {
		"Content-Type": "application/json",
		"contentType": "application/json",
	}
});

// 初始化配置信息
export default {
	post(url, data = {}, header = {}) {
		let token = uni.getStorageSync("token") 
		if (token) {
			header['bdq-cloud-token'] = token
		}
		// 请求拦截 
		return new Promise((resolve, reject) => {
			http.post(url, data, {
				header,
			}).then(res => {
				console.log('请求信息',res);
				if (res['header']['bdq-cloud-token']) {
					uni.setStorage({
						key: "token",
						data: res['header']['bdq-cloud-token']
					})
				}
				if (res.data.errorCode == 401) {
					uni.hideLoading();
					uni.showModal({
						title: "温馨提示",
						content: "登录已过期，请重新登录",
						showCancel: false,
						confirmText: "前往登录",
						success() {
							uni.reLaunch({
								url: "/pages/login/login"
							})
						}
					})
				} else if (res.data.errorCode == 500) {
					console.log(res);
					uni.hideLoading();
					uni.showModal({
						title: "温馨提示",
						content: res.data.errorMessage,
						showCancel: false,
					})
				}
				resolve(res.data);
			}).catch(err => {
				reject(err);
				uni.hideLoading();
				 uni.showModal({
				 	title: "请求服务器错误",
				 	content: JSON.stringify(err),
				 	showCancel: false,
				 })
			})
		})
	},
	get(url, data = {}, header = {}) {
		return http.get(url, {
			header,
			params: data
		})
	}
}