import loginApi from './api/login.js';
import userApi from './api/user.js';
import commonApi from './api/common.js';
import mapApi from './api/map.js';
import indexApi from './api/index.js';
import orderApi from './api/order.js';

export default {
    ...loginApi, ...userApi, ...commonApi, ...mapApi,...indexApi,...orderApi
}