

function checkPermission(array) {//title，权限未开启提示的消息
    return new Promise((resolve) => {
        plus.android.requestPermissions(
            array,//这里面放需要的权限,我这边主要是打电话以及通讯录的一些操作权限
            function (resultObj) {
                if (resultObj.granted.length < 4) { //resultObj.granted是授权的权限，是一个数组，你可以直接判断长度也可以拿出来单独判断
                  uni.showToast({
                        icon: "none",
                        title,
                    });
                    resolve(false)
                    let timer1 = setTimeout(() => { //没有开对应的权限，打开app的系统权限管理页
                        var Intent = plus.android.importClass("android.content.Intent");
                        var Settings = plus.android.importClass("android.provider.Settings");
                        var Uri = plus.android.importClass("android.net.Uri");
                        var mainActivity = plus.android.runtimeMainActivity();
                        var intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        var uri = Uri.fromParts("package", mainActivity.getPackageName(), null);
                        intent.setData(uri);
                        mainActivity.startActivity(intent);
                        clearTimeout(timer1)
                        timer1 = null
                    }, 1000)
                } else {
                    resolve(true)
                }
            }
        );
    })
}


export default checkPermission;
