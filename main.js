import App from './App'
import store from './store'
import Vue from 'vue'
// 引入全局TuniaoUI
import TuniaoUI from 'tuniao-ui'
import libs_utils from "@/libs/mixin/uotils";
import comm_utils from "@/util/utils.js";
import api from '@/common/request/index';
import mixin_utils from '@/common/commom_utils.js'
Vue.config.productionTip = false
App.mpType = 'app'
Vue.use(TuniaoUI)

Vue.prototype.$api = api;
Vue.prototype.$utils = comm_utils;

// 引入TuniaoUI提供的vuex简写方法
let vuexStore = require('@/store/$tn.mixin.js')
Vue.mixin(vuexStore)

// 引入TuniaoUI对小程序分享的mixin封装
let mpShare = require('tuniao-ui/libs/mixin/mpShare.js')
Vue.mixin(libs_utils) //混入常用文件
Vue.mixin(mpShare)
Vue.mixin(mixin_utils)
const app = new Vue({
	store,
	...App
})

// 引入请求封装
require('./util/request/index')(app)

app.$mount()