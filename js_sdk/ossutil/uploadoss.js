const env = require('./config.js'); //配置文件，在这文件里配置你的OSS keyId和KeySecret,timeout:87600; 
const base64 = require('./base64.js'); //Base64,hmac,sha1,crypto相关算法
require('./hmac.js');
require('./sha1.js');
const Crypto = require('./crypto.js');
import Config from  '../../common/config.js'; 

const baseHost = "https://bdq-cloud.oss-cn-shenzhen.aliyuncs.com";
const baseTokenHost =Config.baseURL+ '/aliyun/sts/get';

function computeSignature(accessKeySecret, policyBase64) {
    const accesskey = accessKeySecret;
    const bytes = Crypto.HMAC(Crypto.SHA1, policyBase64, accesskey, {
        asBytes: true
    });
    const signature = Crypto.util.bytesToBase64(bytes);
    return signature;
}

function setPolicyText() {
    const date = new Date();
    const policyText = {
        expiration: date.toISOString(), // 设置policy过期时间。
        conditions: [
            // 限制上传大小。
            ["content-length-range", 0, 1024 * 1024 * 1024],
        ],
    };
    console.log(policyText);
    return policyText;
}

// 生成文件保存名称
function setFilePathUrl(url) {
    let savaFilepath = url.substring(1, url.length)
    return savaFilepath;
}

function getFormDataParams(savePath) {
    return new Promise((resolve, reject) => {
        uni.request({
            url: baseTokenHost,
            method: "POST",
            success(res) {
                const credentialsData = res.data.data;
                const policy = base64.encode(JSON.stringify(
                    setPolicyText())); // policy必须为base64的string。
                const signature = computeSignature(credentialsData.accessKeySecret, policy)
                const formData = {
                    key: setFilePathUrl(savePath), //文件保存名称
                    OSSAccessKeyId: credentialsData.accessKeyId,
                    signature,
                    policy,
                    'x-oss-security-token': credentialsData.securityToken,
                    'success_action_status': '200',
                }
                resolve(formData);
            },
            fail(err) {
                reject(err);
                uni.showToast({
                    title: "获取上传参数失败",
                    icon: "none",
                })
            }
        })
    })
}

function submitOss(obj) {
    return new Promise((resolve, reject) => {
        if (!obj.filePath || obj.filePath.length < 9) {
            uni.showModal({
                title: '图片错误',
                content: '请重试',
                showCancel: false,
            })
            reject({
                "statusCode": 500,
                "errMsg": "uploadFile:error"
            });
            return;
        }
        getFormDataParams(obj.savePath).then(ParamsConfig => {
            console.log('ParamsConfig:::', {
                url: baseHost,
                filePath: obj.filePath, //图片资源路径
                name: 'file', // 必须填file。
                formData: ParamsConfig,
            });
            uni.uploadFile({
                url: baseHost,
                filePath: obj.filePath, //图片资源路径
                name: 'file', // 必须填file。
                formData: ParamsConfig,
                success(res) {
                    resolve(res);
                },
                fail(err) {
                    uni.showToast({
                        title: "请求错误",
                        icon: "none",
                    })
                }
            });
        })
    })

}

module.exports = {
    getFormDataParams,
    submitOss,
    computeSignature
}